package org.telemoney;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.client.RestTemplate;

/**
 * Servlet implementation class RequestRefIDServlet
 */
public class RequestRefIDServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final int[] LETTERS = {4,5,6,7,8,9,0,1,2,3};
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RequestRefIDServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RestTemplate restTemplate = new RestTemplate();
		String gtw = request.getParameter("gtw");
//        MultiValueMap<String, Object> mm = new LinkedMultiValueMap<>();
//        mm.add("gtw", gtw);
//		Map<String, Object> mm = new HashMap<>();
//		mm.put("gtw", gtw);
//		String  s = restTemplate.getForObject("http://localhost:9001/yacceleratorstorefront/insurance/en/msigcommonpay/requestRefid?gtw=" + gtw , String.class);
		String  s = restTemplate.getForObject("http://117.121.10.36:8080/yacceleratorstorefront/insurance/en/msigcommonpay/requestRefid", String.class);
//        SimpleDateFormat sf = new SimpleDateFormat("yyMMddHHmmss");
//		request.setAttribute("refid", sf.format(new Date()));
//		SimpleDateFormat sf = new SimpleDateFormat("yyMMddHHmmssSSS");
//		String rawStr = sf.format(new Date());
//		String refid = generateLetters(4) + maskCurrentTime(rawStr);
		request.setAttribute("refid", s);
        request.setAttribute("gtw", gtw);
        request.getRequestDispatcher("CommonPay_Step_1.jsp").forward(request, response);
	}
	
	private static final String[] UPPERLETTERS = {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
	
	private static String generateLetters(int count){
		List<Integer> randoms = new ArrayList<>();
		IntStream.range(0,count).forEach(s -> {
			long seed = java.util.UUID.randomUUID().getLeastSignificantBits();
			Random random = new Random(seed);
//			System.out.println(random.nextInt(51));
			randoms.add(random.nextInt(26));
		});
		String sss = randoms.stream().map(s -> UPPERLETTERS[s]).collect(Collectors.joining());
//		System.out.println(sss);
		return sss;
	}
	
	private static String maskCurrentTime(String rawStr){
		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < rawStr.toCharArray().length; i++){
//			System.out.println(rawStr.toCharArray()[i]);
			String j = String.valueOf(rawStr.toCharArray()[i]);
			sb.append(LETTERS[Integer.parseInt(j)]);
		}
		return sb.toString();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
