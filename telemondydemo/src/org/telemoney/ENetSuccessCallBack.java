package org.telemoney;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.wiz.enets2.transaction.umapi.CreditMerchant;
import com.wiz.enets2.transaction.umapi.Merchant;
import com.wiz.enets2.transaction.umapi.data.CreditTxnRes;
import com.wiz.enets2.transaction.umapi.data.TxnRes;

/**
 * Servlet implementation class ENetSuccessCallBack
 */
public class ENetSuccessCallBack extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ENetSuccessCallBack() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Enets return page(Asynchronous...)");
//		System.out.println(new Date() + "invokde status url get method here....");
//		
//		System.out.println("-----attributes-----");
//		Collections.list(request.getAttributeNames()).forEach(s -> {
//			System.out.println(s + "-----" + request.getAttribute(s));
//		});
//		
//		System.out.println("-----headers-----");
//		Collections.list(request.getHeaderNames()).forEach(s -> {
//			System.out.println(s + "-----" + request.getHeader(s));
//		});
//		
//		System.out.println("-----request-----");
//		
//		request.getParameterMap().forEach((x, y) -> {
//			System.out.println(x + "----" + Arrays.asList(y).stream().collect(Collectors.joining("|")));
//		});
		
		String sMsg = request.getParameter("message");
		CreditMerchant m = (CreditMerchant) Merchant.getInstance(Merchant.MERCHANT_TYPE_CREDIT);
		TxnRes res = m.unpackResponse(sMsg);
		CreditTxnRes credRes = res.getCreditTxnRes();
		if (credRes == null)
			credRes = new CreditTxnRes();
		
		request.setAttribute("mid", res.getNetsMid());
		request.setAttribute("merchant_txn_ref", res.getMerchantTxnRef());
		request.setAttribute("nets_txn_ref", res.getNetsTxnRef());
		request.setAttribute("nets_txn_dtm", res.getNetsTxnDtm());
		request.setAttribute("nets_txn_status", res.getNetsTxnStatus());
		request.setAttribute("nets_txn_resp_code", res.getNetsTxnRespCode());
		request.setAttribute("nets_txn_msg", res.getNetsTxnMsg());
		request.setAttribute("nets_amt_deducted", credRes.getNetsAmountDeducted());
		request.setAttribute("bank_auth_id", credRes.getBankAuthId());
	
		request.getRequestDispatcher("enetssuccess.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
