package org.telemoney;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class UnifiedSuccessCallBackServlet
 */
public class UnifiedSuccessServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UnifiedSuccessServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		out.println("<html><header></headder><body>");
		out.println("<h2>attributes</h2>");
		Enumeration<String> attributes = request.getAttributeNames();
		while(attributes.hasMoreElements()){
			String key = attributes.nextElement();
			out.println(key + "-----" + request.getAttribute(key));
			out.println("<br/>");
		}
		out.println("<br/>");
		
		out.println("<h2>-----headers-----</h2>");
		Enumeration<String> headerNames = request.getHeaderNames();
		while(headerNames.hasMoreElements()){
			String key = headerNames.nextElement();
			out.println(key + "-----" + request.getHeader(key));
			out.println("<br/>");
		}
		out.println("<br/>");
		
		out.println("<h2>-----request-----</h2>");
		Map<String,String[]> map = request.getParameterMap();
		for(Map.Entry<String, String[]> entry : map.entrySet()){
			out.println(entry.getKey() + "----" + Arrays.asList(entry.getValue()).stream().collect(Collectors.joining(" | ")));
			out.println("<br/>");
		}
		out.println("<br/>");
		out.println("</body></html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
