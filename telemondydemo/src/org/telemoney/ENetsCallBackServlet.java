package org.telemoney;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ENetsCallBackServlet
 */
public class ENetsCallBackServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ENetsCallBackServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Enets return page(Asynchronous...)");
		System.out.println(new Date() + "invokde status url get method here....");
		
		System.out.println("-----attributes-----");
		Collections.list(request.getAttributeNames()).forEach(s -> {
			System.out.println(s + "-----" + request.getAttribute(s));
		});
		
		System.out.println("-----headers-----");
		Collections.list(request.getHeaderNames()).forEach(s -> {
			System.out.println(s + "-----" + request.getHeader(s));
		});
		
		System.out.println("-----request-----");
		
		request.getParameterMap().forEach((x, y) -> {
			System.out.println(x + "----" + Arrays.asList(y).stream().collect(Collectors.joining("|")));
		});
		
//		String mid = request.getParameter("mid");
//		String ref = request.getParameter("ref");
//		String status = request.getParameter("status");
//		
//		
//		String back = "mid=" + mid + "&ref=" + ref + "&ack=" + status;
//		System.out.println(new Date() + "write response... " + back);
////		response.getWriter().print(back);
//		response.getWriter().write(back);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
