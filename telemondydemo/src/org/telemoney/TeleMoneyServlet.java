package org.telemoney;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class TeleMoneyServlet
 */
public class TeleMoneyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TeleMoneyServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();	
		String gtw = request.getParameter("gtw");
		request.setAttribute("gtw", gtw);
		String portalCode = request.getParameter("portalCode");
		request.setAttribute("portalCode", gtw);
		out.println("<html><header></headder><body>");
		out.println("<h1>" + gtw +"</h1>");
		out.println("<br/>");
		out.println("<h1>" + portalCode +"</h1>");
		out.println("Return page(Synchronized...)");
		out.println("<br/>");
		out.println(new Date() + "invokde return url get method here....");
		out.println("<br/>");
		out.println("<h2>attributes</h2>");
		Enumeration<String> attributes = request.getAttributeNames();
		while(attributes.hasMoreElements()){
			String key = attributes.nextElement();
			out.println(key + "-----" + request.getAttribute(key));
			out.println("<br/>");
		}
		out.println("<br/>");
		
		out.println("<h2>-----headers-----</h2>");
		Enumeration<String> headerNames = request.getHeaderNames();
		while(headerNames.hasMoreElements()){
			String key = headerNames.nextElement();
			out.println(key + "-----" + request.getHeader(key));
			out.println("<br/>");
		}
		out.println("<br/>");
		
		out.println("<h2>-----request-----</h2>");
		Map<String,String[]> map = request.getParameterMap();
		for(Map.Entry<String, String[]> entry : map.entrySet()){
			out.println(entry.getKey() + "----" + Arrays.asList(entry.getValue()).stream().collect(Collectors.joining(" | ")));
			out.println("<br/>");
		}
		out.println("<br/>");
		out.println("</body></html>");
//		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		out.println(new Date() + "invokde return url post method here....");
		doGet(request, response);
	}

}
