<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Common Pay Demo</title>
<script type="text/javascript" src="js/jquery-2.2.0.js"></script>
<script type="text/javascript">
function goNext(){
	var refid = $("input[name='refid']").val();
	/* var gtw = $("select[name='gtw']").val(); */
	window.location.href = "CommonPay_Step_2.jsp?ref=" + refid;
}
</script>
</head>
<body>
<form method="post" action="RequestRefIDServlet">
<table align="center" border=1 width=725 style="border-collapse:collapse;">
	<th colspan=3 >Common Pay Demo Step 1</th>
	<%-- <tr>
		<td width=161 height=27>GTW</td><td width=455 align="left">
			<select name="gtw">
				<option value="TELE" <c:if test="${gtw eq 'TELE' }">selected</c:if>>Telemoney</option>
				<option value="ENETS" <c:if test="${gtw eq 'ENETS' }">selected</c:if>>eNets</option>
			</select>
		</td>
	</tr> --%>
	<tr><td width=161 height=27>REF ID</td>
		<td width=455 align="left"><input type="text" name="refid" value="${refid}" style="width: 200px"/></td>
		<td colspan="3" align="center" height=27><input type="submit" value="Request RefID"/></td>
	</tr>
	<tr><td colspan="3" align="center" height=27><a id="next" href="#" onclick="javascript:goNext()">Go Step 2</a></td></tr>
</table>
</form>
</body>
</html>