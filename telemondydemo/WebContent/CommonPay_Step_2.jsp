<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<script src="js/jquery-2.2.0.js"></script>
<title>Common Pay Demo</title>
<script>
$( document ).ready(function() {
	$gtwSelection = $('select[name="gtw"]');
	var gtw = $gtwSelection.val();
	setParas(gtw);
	$gtwSelection.on('change',function(){
    	var gtw = $(this).val();
    	setMid(gtw);
    	setAmt(gtw);
    	setPaymentMode(gtw);
    })
});
var setParas = function(gtw){
	setMid(gtw);
	setAmt(gtw);
	setPaymentMode(gtw);
}
var setMid = function(gtw){
	if(gtw == 'TELE'){
		$('input[name="mid"]').val('20150609001');
	}else if(gtw == 'ENETS'){
		$('input[name="mid"]').val('888467000');
	}
}
var setAmt = function(gtw){
	if(gtw == 'TELE'){
		$('input[name="amt"]').val('123.00');
	}else if(gtw == 'ENETS'){
		$('input[name="amt"]').val('20000');
	}
}
var setPaymentMode = function(gtw){	
	var creditOP = "<option value=\"CC\">CC (CreditCard)</option>";
	var debitOP = "<option value=\"DE\">DE (Debit)</option>";
	$paymentMode = $('select[name="paymentMode"]');
	$paymentMode.find('option').remove();
	if(gtw == 'TELE'){
		$paymentMode.append(creditOP);
	}else if(gtw == 'ENETS'){
		$paymentMode.append(creditOP);
		$paymentMode.append(debitOP);
	}
}
/* var setUrls = function(gtw){
	if(gtw == 'TELE'){
		$('input[name="successUrl"]').val('123.00');
		$('input[name="failureUrl"]').val('123.00');
		$('input[name="callbackUrl"]').val('123.00');
	}else if(gtw == 'ENETS'){
		$('input[name="successUrl"]').val('123.00');
		$('input[name="failureUrl"]').val('123.00');
		$('input[name="callbackUrl"]').val('123.00');
	}
} */
</script>
</head>
<body>
<!-- <form name=form action="https://test.wirecard.com.sg/easypay2/paymentpage.do" method="post" OnSubmit="true"> -->
<!-- <form name="form" action="http://117.121.10.36:8080/msigpaymentgateway/requestPay" method="post"> -->
<!-- <form name="form" action="http://localhost:9001/msigpaymentgateway/requestPay" method="post"> -->
<!-- <form name="form" action="http://117.121.10.36:8080/yacceleratorstorefront/insurance/en/msigcommonpay/requestPay" method="post"> -->
<form name="form" action="http://localhost:9001/yacceleratorstorefront/insurance/en/msigcommonpay/requestPay" method="post">
<% 
String 	refid			= request.getParameter("ref").trim();
%>
<%-- <c:set var="gtw" value="<%=gtw%>"></c:set> --%>
<table align="center" border=1 width=725 style="border-collapse:collapse;">
	<th colspan=3 height=27>Common Pay Demo Step 2</th>
	<tr><td width=161 height=27>QuoteNum</td><td width=455 align="left"><input type="text" name="quoteNum" style="width : 200px"/></td></tr>
	<tr>
		<td width=161 height=27>PortalCode</td>
		<td width=455 align="left">
			<select name="portalCode" style="width : 200px">
				<option value="CP">CP</option>
				<option value="IP">IP</option>
			</select>
		</td>
	</tr>
	<tr>
		<td width=161 height=27>ChanCode</td>
		<td width=455 align="left">
			<select name="chanCode" style="width : 200px">
				<option value="DBS">DBS</option>
				<option value="MSIG">MSIG</option>
				<option value="SCB">SCB</option>
			</select>
		</td>
	</tr>
	<tr>
		<td width=161 height=27>CatCode</td>
		<td width=455 align="left">
			<select name="catCode" style="width : 200px">
				<option value="PA">PA</option>
				<option value="MOTOR">MOTOR</option>
			</select>
		</td>
	</tr>
	<tr>
		<td width=161 height=27>PolicyType</td>
		<td width=455 align="left">
			<select name="policyType" style="width : 200px">
				<option value="DAT">DAT</option>
			</select>
		</td>
	</tr>
	<tr>
		<td width=161 height=27>GTW</td><td width=455 align="left">
			<select name="gtw">
				<option value="TELE" >Telemoney</option>
				<option value="ENETS" >eNets</option>
			</select>
		</td>
	</tr>
	<tr><td width=161 height=27>Mid</td><td width=455 align="left"><input type="text" name="mid" size="22" style="width : 200px"/></td></tr>
	<tr><td width=161 height=27>Ref</td><td width=455 align="left"><input type="text" name="refid" size="20" value="<%=refid%>" style="width : 200px" readonly/></td></tr>
	<tr><td width=161 height=27>Cur</td><td><input type="text" name="cur" size="22" maxlength="20" value="SGD" style="width : 200px"/></td></tr>
	<tr><td width=161 height=27>Amt</td><td><input type="text" name="amt" size="22" maxlength="12" style="width : 200px"/></td></tr>
	<tr>
		<td width=161 height=27>PaymentMode</td>
		<td width=455 align="left">
			<select name="paymentMode" style="width : 200px">
				<!-- <option value="CC">CC (CreditCard)</option> -->
			</select>
		</td>
	</tr>
	<tr>
		<td width=161 height=27>PaymentType</td>
		<td width=455 align="left">
			<select name="paymentType" style="width : 200px">
				<option value="AUTH">AUTH</option>
				<option value="SALE">SALE</option>
			</select>
		</td>
	</tr>
	<tr>
		<td width=161 height=27>CardType</td>
		<td width=455 align="left">
			<select name="cardType" style="width : 200px">
				<option value=""></option>
				<option value="2">Master Credit Card</option>
				<option value="3">VISA Credit Card</option>
				<!-- <option value="5">Amex Credit Card</option>
				<option value="22">Diners Credit Card</option>
				<option value="23">JCB Credit Card</option>
				<option value="25">China UnionPay card</option>
				<option value="41">ENets</option> -->
			</select>
		</td>
	</tr>
	<tr>
		<td width=161 height=27>SuccessURL</td><td><input type="text" name="successUrl" value="http://117.121.10.36:18080/telemondydemo/UnifiedSuccessServlet" style="width : 500px"/></td>
	</tr>
	<tr>
		<td width=161 height=27>FailureURL</td><td><input type="text" name="failureUrl" value="http://117.121.10.36:18080/telemondydemo/UnifiedFailureServlet" style="width : 500px"/></td>
	</tr>
	<tr>
		<td width=161 height=27>CallbackURL</td><td><input type="text" name="callbackUrl" value="http://117.121.10.36:18080/telemondydemo/UnifiedCallBackServlet" style="width : 500px"/></td>
	</tr>
	<tr><td colspan="3" align="center" height=27><input type="submit" name="Submit" value="GO NEXT"/></td></tr>
</table>
</form>
</body>
</html>